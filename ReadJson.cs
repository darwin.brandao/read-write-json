﻿using System.Text.Json;

namespace ReadWriteJson;
public class ReadJson
{

    private string filePath;

    public ReadJson()
    {
        filePath = string.Empty;
    }

    public ReadJson(string filePath)
    {
        this.filePath = filePath;
    }

    public T Read<T>(string json)
    {
        T result = JsonSerializer.Deserialize<T>(json);
        return result;
    }

    public T Read<T>(FileInfo fileInfo)
    {
        if (File.Exists(fileInfo.FullName))
            return Read<T>(File.ReadAllText(fileInfo.FullName));
        return default(T);
    }

    public T Read<T>()
    {
        if (File.Exists(filePath))
        {
            string content = File.ReadAllText(filePath);
            return Read<T>(content);
        }
        return default(T);
    }
}
