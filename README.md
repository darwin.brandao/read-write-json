# Read Write Json

This is just a simple wrapper to System.Text.Json in .Net Core 6.0.
This is not a project, I'm publishing it here just to keep it backed up.

Use it as you want to. No attributions needed.

## License
MIT License.
