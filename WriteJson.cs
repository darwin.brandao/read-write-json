using System.Text.Json;

namespace ReadWriteJson;
public class WriteJson
{

    private string filePath;

    public WriteJson()
    {
        filePath = string.Empty;
    }

    public WriteJson(string filePath)
    {
        this.filePath = filePath;
    }
    
    public void Write<T>(T obj)
    {
        string content = JsonSerializer.Serialize<T>(obj, new JsonSerializerOptions() { WriteIndented = true });
        Write(filePath, content);
    }

    public void Write(string filePath, string content)
    {
        File.WriteAllText(filePath, content);
    }

    public void Write<T>(string filePath, T obj)
    {
        string content = JsonSerializer.Serialize<T>(obj, new JsonSerializerOptions() { WriteIndented = true });
        Write(filePath, content);
    }
}
